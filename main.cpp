#include <iostream>
#include "tests.h"
#include "headers/game.h"

using namespace std;

//main function
int main() {
    //runTests();
    Game game = Game(2); //setup 2 player game, currently only argument allowed to be passed.
    game.PlayGame();
    return 0;
}