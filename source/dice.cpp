// dice.cpp
// implementation of dice.h
//
#include <ctime>
#include <stdlib.h>
#include "../headers/dice.h"

//implementation of constructor for a dice.
Dice::Dice() {
    srand(time(NULL));
}


//implementation of rolling a dice
void Dice::rollDie() {
    Dice::roll =  rand() % 6 + 1;
}


//implementation of getRoll getter function
int Dice::getRoll() {
    return Dice::roll;
}


//implementation of setRoll tester method
void Dice::setRoll(int rollVal) {
    Dice::roll = rollVal;
}

