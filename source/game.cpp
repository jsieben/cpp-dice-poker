//
// Implementation for Game
//

#include <iostream>
#include "../headers/game.h"


Game::Game(int numPlayers) {
    totalPlayers = numPlayers;
    playing = false;
}

void Game::PlayGame() {
    playing = true;
    std::string input = "";
    bool validInput = false;
    int currentPlayer = 0;
    int throwCount = 0;
    int diceToRoll[5] = {0, 0, 0, 0, 0};
    int* playerHand;

    while (playing) {
        //initial throw of the dice
        if (throwCount == 0) {
            //output header
            std::cout << std::endl <<"*********************************************" << std::endl;
            std::cout << "New Game";
            std::cout << std::endl <<"*********************************************" << std::endl;
            //set diceToRoll to all one's, will make the first roll throw every dice.
            for(int i = 0; i < 5; i++)
                diceToRoll[i] = 1;
            //Roll both player's dice and get their hands
            for(int j = 0; j < totalPlayers; j++) {
                player[currentPlayer].RollDice(diceToRoll);
                playerHand = player[currentPlayer].GetHand();
                //Print out both players hands and tell them what they have.
                std::cout << "Player " << (currentPlayer + 1) << ":  ";
                for (int k = 0; k < 5; k++) {
                    std::cout << playerHand[k] << ", ";
                }
                std::cout << " : " << HandRank(player[currentPlayer]) << std::endl;
                currentPlayer += 1;
            }
            throwCount += 1;
            std::cout <<"-------------------------------------------------" << std::endl; // Output formatting
        }
        // Second throw, First re-roll chance. Third throw, Second re-roll chance.
        else if(throwCount == 1 || throwCount == 2) {
            for (int currentPlayer = 0; currentPlayer < totalPlayers; currentPlayer++) {
                //Let the player choose dice to re-roll
                std::cout << "Player " << (currentPlayer + 1) << " do you want to re-roll..." << std::endl;
                //Ask the player for each dice if they want to re-roll
                for (int i = 0; i < 5; i++) {
                    std::cout << "dice " << (i + 1) << "? y/n: ";
                    std::cin >> input;
                    //exit condition
                    if (input == "exit") {
                        playing = false;
                        goto exit;
                    }
                    //Get the user input and set diceToRoll to 1 for y and 0 for n
                    validInput = false;
                    while(!validInput)
                    if(input == "y") {
                        diceToRoll[i] = 1;
                        validInput = true;
                    }
                    else if(input == "n") {
                        diceToRoll[i] = 0;
                        validInput = true;
                    }
                    else { //check for invalid inputs
                        std::cout << "Invalid input, please input valid y/n. : ";
                        std::cin >> input;
                    }
                }
                std::cout << std::endl <<"-------------------------------------------------" << std::endl;
                //re-roll the player's hand who just choose dice to re-roll
                player[currentPlayer].RollDice(diceToRoll);
                //print out the players hand before going onto the next player to re-roll dice.
                for(int j = 0; j < totalPlayers; j++) {
                    playerHand = player[j].GetHand();
                    std::cout << "Player " << (j + 1) << ":  ";
                    for (int k = 0; k < 5; k++) {
                        std::cout << playerHand[k] << ", ";
                    }
                    std::cout << " : " << HandRank(player[j]) << std::endl;
                }
                std::cout <<"-------------------------------------------------" << std::endl;
            }
            throwCount += 1;
        }
        // Figure out who won
        else {
            if (player[0].Score() > player[1].Score())
                std::cout << "Player 1 Wins!!!";
            else if (player[0].Score() < player[1].Score())
                std::cout << "Player 2 Wins!!!";
            else
                std::cout << "The Game Is A Tie!!!";
            std::cout << std::endl <<"-------------------------------------------------" << std::endl;
            //reset the game
            throwCount = 0;
            currentPlayer = 0;
        }
        exit:;
    }
}

// Get a string value for the player's score.
std::string Game::HandRank(Hand hand) {
    if (hand.Score() > 800000)
        return "Five Of A Kind";
    if (hand.Score() > 700000)
        return "Four Of A Kind";
    if (hand.Score() > 600000)
        return "Full House";
    if (hand.Score() > 500000)
        return "Straight";
    if (hand.Score() > 400000)
        return "Three Of A Kind";
    if (hand.Score() > 300000)
        return "Two Pair";
    if (hand.Score() > 200000)
        return "One Pair";
    if (hand.Score() > 100000)
        return "High Die";
}
