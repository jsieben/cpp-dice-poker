// hand.cpp
// Implementation of hand.h
//
#include <algorithm>
#include <iostream>
#include "../headers/hand.h"

//implementation of constructor
Hand::Hand() {
    for (int i = 0; i < 5; i++) {
        hand[i].rollDie(); //roll each dice once on creation of hand
    }
}

void Hand::RollDice(int *needToRoll) {
    for (int i = 0; i < 5; i++) {
        if (needToRoll[i] == 1)
            hand[i].rollDie(); //re-roll the die on a 1
        else
            continue; //continue on anything else
    }
}

int* Hand::GetHand() {
    static int returnHand[5] = {0, 0, 0, 0, 0};
    for (int i = 0; i < 5; i++)
        returnHand[i] = hand[i].getRoll();
    return returnHand;
}

int Hand::Score() {
    int score = 0;
    int dieValue[5];
    int scoreValue[8];
    int elements = sizeof(dieValue) / sizeof(dieValue[0]);
    for (int i = 0; i < 5; i++) {
        dieValue[i] = hand[i].getRoll();
    }
    std::sort(dieValue, dieValue + elements); //sort the hand array

    //setup scoreValue array
    scoreValue[0] = HighCard(dieValue);
    scoreValue[1] = Pair(dieValue);
    scoreValue[2] = TwoPair(dieValue);
    scoreValue[3] = ThreeOfKind(dieValue);
    scoreValue[4] = Straight(dieValue);
    scoreValue[5] = FullHouse(dieValue);
    scoreValue[6] = FourOfKind(dieValue);
    scoreValue[7] = FiveOfKind(dieValue);
    //find highest Score and return
    score = scoreValue[0];
    for(int j = 1; j < 8; j++) {
        if (score < scoreValue[j])
            score = scoreValue[j];
        else
            continue;
    }
    return score;
}


//implementation of individual hand scoring. Private method.
//implementation for HighCard
int Hand::HighCard(int *hand) {
    int score = 0;
    //step backwards through sorted array
    for(int i = 4; i >= 0; i--) {
        score += hand[i] * (int)pow(10, i);
    }
    return (100000 + score);
}

//implementation for Pair
int Hand::Pair(int *hand) {
    int score = 0;
    int position = 1000;
    bool isPair = false;
    //step backward through sorted array
    for(int i = 4; i >= 1; i--) {
        if (hand[i] == hand[i-1]) {
            if (!isPair) {
                isPair = true;
                score += hand[i] * 10000;
                continue;
            }
        }
        score += hand[i] * position;
        position /= 10;
    }
    score += hand[0];
    if(isPair) {return (200000 + score);}
    else {return 0;}
}

//implementation for TwoPair
int Hand::TwoPair(int *hand) {
    int score = 0;
    int position = 100;
    bool firstPair = false;
    bool secondPair = false;
    //step backward through sorted array
    for(int i = 4; i >= 1; i--) {
        if (hand[i] == hand[i - 1]) {
            if (!firstPair) {
                firstPair = true;
                score += hand[i] * 10000;
                continue;
            }
            else if(!secondPair) {
                secondPair = true;
                score += hand[i] * 1000;
                continue;
            }
        }
        score += hand[i] * position;
        position /= 10;
    }
    score += hand[0];
    if(firstPair && secondPair) {return (300000 + score);}
    else {return 0;}
}

//implementation for ThreeOfKind
int Hand::ThreeOfKind(int *hand) {
    int score = 0;
    int position = 1000;
    bool isThree = false;
    //step backward through sorted array
    for(int i = 4; i >= 2; i--) {
        if(hand[i] == hand[i - 1] && hand[i] == hand[i - 2]) {
            if (!isThree) {
                isThree = true;
                score += hand[i] * 10000;
                continue;
            }
        }
        score += hand[i] * position;
        position /= 10;
    }
    score += ((hand[1] * 10) + hand[0]);
    if (isThree) {return (400000 + score);}
    else {return 0;}
}

//implementation for Straight
int Hand::Straight(int *hand) {
    int score = 0;
    bool isStraight = false;
    //step backward through sorted array
    for(int i = 4; i >= 1; i--) {
        if((hand[i] - 1) == hand[i - 1]) {
            isStraight = true;
        }
        else {
            isStraight = false;
            break;
        }
        score += hand[i] * (int)pow(10, i);
    }
    score += hand[0];
    if(isStraight) {return (500000 + score);}
    else {return 0;}
}

//implementation for FullHouse
int Hand::FullHouse(int *hand) {
    int score = 0;
    int threeKind = 0;
    bool isThree = false;
    bool isPair = false;
    //step backward through sorted array
    for(int i = 4; i >= 2; i--) {
        if(hand[i] == hand[i - 1] && hand[i] == hand[i - 2]) {
            if(!isThree) {
                isThree = true;
                score += hand[i] * 10000;
                threeKind = hand[i];
            }
        }
    }
    if(hand[4] != threeKind && hand[4] == hand[3]) {
        score += hand[4] * 1000;
        isPair = true;
    }
    else if(hand[1] != threeKind && hand[1] == hand[0]) {
        score += hand[1] * 1000;
        isPair = true;
    }
    if(isThree && isPair) {return (600000 + score);}
    else {return 0;}
}

//implementation for FourOfKind
int Hand::FourOfKind(int *hand) {
    int score = 0;
    bool isFour = false;
    //step backward through sorted array
    for (int i = 4; i >= 3; i--) {
        if (hand[i] == hand[i - 1] && hand[i] == hand[i - 2]
                && hand[i] == hand[i - 3]) {
            isFour = true;
            score += hand[i] * 10000;
            continue;
        }
        if(i == 4)
            score += hand[4] * 1000;
        else if(i == 3)
            score += hand[0] * 1000;
    }
    if(isFour) {return (700000 + score);}
    else {return 0;}
}

//implementation for FiveOfKind
int Hand::FiveOfKind(int *hand) {
    int score = 0;
    bool isFive = false;
    //check if all dice are the same
    if (hand[4] == hand[3] && hand[4] == hand[2]
            && hand[4] == hand[1] && hand[4] == hand[0]) {
        isFive = true;
        score += hand[4] * 10000;
    }
    if(isFive) {return (800000 + score);}
    else {return 0;}
}