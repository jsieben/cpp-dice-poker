//
// For testing the hand class
// Added ability to directly set the player hand

#include <iostream>
#include "headers/hand.h"

#ifndef HW13_TESTHAND_H
#define HW13_TESTHAND_H
class TestHand : public Hand {
    //construct with specific roll, uses Dice.setRoll/
public:
    TestHand(int roll[]) {
        for (int i = 0; i < 5; i++) {
            hand[i].setRoll(roll[i]);
        }
    }

    void SetHand(int roll[]) {
        for (int i = 0; i < 5; i++) {
            hand[i].setRoll(roll[i]);
        }
    }

    void printHand() {
        for (int i = 0; i < 5; i++)
            std::cout << hand[i].getRoll();
    }
};
#endif //HW13_TESTHAND_H
