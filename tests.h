//
// Test cases
//

#ifndef HW13_TESTS_H
#define HW13_TESTS_H
#include <iostream>
#include "headers/dice.h"
#include "testHand.h"

using namespace std;

//test functions
//test the Dice class
bool testDie() {
    int count[7] = {0, 0, 0, 0, 0, 0, 0};
    Dice die;
    bool result = false;
    for(int i = 0; i < 1000000; i++) {
        die.rollDie();
        count[die.getRoll()] += 1;
    }

    for(int i = 1; i <= 6; i++) {
        //cout << i << "'s: " << count[i] << endl;
        double percent = count[i]/1000000.0;
        if (percent < 0.17) { //expected each dice should show up about 16.67% of the time
            result = true;
        }
        else {
            cout << percent << " " << i << endl;
            cout << (percent < 0.17);
            return false;
        }
    }
    return result;
}


//test the Hand class with TestHand to set specific values
bool testHand() {
    bool result = false;
    int newRoll[] = {0, 1, 0, 1, 0};
    int highCard[] = {2, 3, 5, 6, 1};
    int pair[] = {3, 1, 2, 3, 4};
    int twoPair[] = {3,1,2,3,1};
    int threeOfKind[] = {4, 1, 4, 4, 6};
    int straight[] = {5, 4, 3, 2, 1};
    int fullHouse[] = {4, 6, 4, 6, 6};
    int fourOfKind[] = {1, 2, 1, 1, 1};
    int fiveOfKind[] = {2, 2, 2, 2, 2};
    TestHand testHand(highCard);

    //test that RollDice works and GetHand works
    testHand.RollDice(newRoll);
    int* newHand = testHand.GetHand();
    int samecount = 5;
    for(int i = 0; i < 5; i++) {
        if (highCard[i] != newHand[i])
            samecount -= 1;
    }
    if (samecount != 5)
        result = true;
    else return false;

    //test the high card scoring
    testHand.SetHand(highCard);
    if (testHand.Score() == 165321)
        result = true;
    else return false;

    //test the pair scoring
    testHand.SetHand(pair);
    if (testHand.Score() == 234321)
        result = true;
    else return false;

    //test the two pair scoring
    testHand.SetHand(twoPair);
    if (testHand.Score() == 331321)
        result = true;
    else return false;

    //test the three of a kind scoring
    testHand.SetHand(threeOfKind);
    if (testHand.Score() == 446441)
        result = true;
    else return false;

    //test the straight scoring
    testHand.SetHand(straight);
    if (testHand.Score() == 554321)
        result = true;
    else return false;

    testHand.SetHand(fullHouse);
    if (testHand.Score() == 664000)
        result = true;
    else return false;

    testHand.SetHand(fourOfKind);
    if (testHand.Score() == 712000)
        result = true;
    else return false;

    testHand.SetHand(fiveOfKind);
    if (testHand.Score() == 820000)
        result = true;
    else return false;

    return result;
}


//run all tests and check if the tests pass
void runTests() {
    cout << "The results of the tests are: " << endl;
    if (testDie()) {
        cout << "Dice Passed" << endl;
    }
    else {
        cout << "Dice Failed";
    }
    if (testHand())
        cout << "Hand Passed" << endl;
    else
        cout << "Hand Failed" << endl;
}
#endif //HW13_TESTS_H
