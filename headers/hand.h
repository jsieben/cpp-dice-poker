// hand.h
// interface for hand object
// it will roll 5 dice and be able to re-roll specific dice in the hand

#ifndef HW13_HAND_H
#define HW13_HAND_H

#include "dice.h"

class Hand {
protected:
    Dice hand[5];
private:
    int HighCard(int hand[]);
    int Pair(int hand[]);
    int TwoPair(int hand[]);
    int ThreeOfKind(int hand[]);
    int Straight(int *hand);
    int FullHouse(int hand[]);
    int FourOfKind(int hand[]);
    int FiveOfKind(int hand[]);
public:
    Hand(); //constructor for a dice hand
    void RollDice(int needToRoll[]); //given an array of 1's and 0's re-roll dice. 1 for re-roll.
    int* GetHand(); //return an int array for the current hand
    int Score(); //return a number based on the hand. 0 for high die, 9 for five of a kind
};
#endif //HW13_HAND_H
