// dice.h
// interface for dice object
// can roll a single dice

#ifndef HW13_DICE_H
#define HW13_DICE_H
class Dice {
private:
    int roll;
public:
    Dice(); //constructor that seeds random number generator
    void rollDie(); //roll the die once
    int getRoll(); //return the value of the die roll '0' means never rolled
    void setRoll(int rollVal); //for testing the hand class
};
#endif //HW13_DICE_H
