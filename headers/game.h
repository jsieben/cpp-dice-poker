// Class for playing the game
// It controls the main game loop
// It will allow two players to play the game.
#include "hand.h"

#ifndef HW13_GAME_H
#define HW13_GAME_H
class Game {
    Hand player[2];
    bool playing;
    int totalPlayers;
private:
    std::string HandRank(Hand hand);
public:
    Game(int numPlayers);
    void PlayGame();
};
#endif //HW13_GAME_H
